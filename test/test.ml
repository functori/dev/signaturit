open Signaturit

[@@@arg {colors}]

type contact_input = contact_content = {
  email: string;
  name: string;
} [@@deriving arg]

type contact_command =
  | Clist
  | Cget of string
  | Cdelete of string
  | Ccreate of contact_input
[@@deriving arg]

type signature_input = {
  s_body: string option;
  s_subject: string option;
  s_email: string; [@req]
  s_name: string; [@req]
  s_data: string list; [@dft []]
  s_templates: string list; [@dft []]
  s_delivery_type: [`url | `sms | `email] option;
} [@@deriving arg]

type signature_command =
  | Slist
  | Scount
  | Sget of string
  | Screate of signature_input
[@@deriving arg]

type command =
  | Contact of contact_command
  | Signature of signature_command
[@@deriving arg]

type config = {
  api: string; [@dft "https://api.sandbox.signaturit.com/v3"]
  access_token: string; [@dft "HDPEInQrdVbxDzgbZdNoyRmOcYAFheRxLQQRoOhDPcICtIJnngQzEdEKfVzBtsQgJmGvpUcNlUtqWmUaFoQVgl"]
  command: command;
} [@@deriving arg {exe="test.exe"}]

let print enc p =
  let|> r = p in
  match r with
  | Error EzReq_lwt_S.KnownError {code; error} ->
    Format.eprintf "Error %d\n%s@." code @@ EzEncoding.construct ~compact:false error_enc error
  | Error EzReq_lwt_S.UnknownError {code; msg} ->
    Format.eprintf "Error %d%s@." code @@ Option.fold ~none:"" ~some:(fun s -> "\n" ^ s) msg
  | Ok x -> Format.printf "%s@." @@ EzEncoding.construct ~compact:false enc x

let () =
  let config = parse_config () in
  api := EzAPI.BASE config.api;
  access_token := config.access_token;
  EzLwtSys.run @@ fun () ->
  match config.command with
  | Contact Clist -> print contacts_enc @@ Contact.list ()
  | Contact Cget id -> print contact_enc @@ Contact.get (Id.mk id)
  | Contact Cdelete id -> print contacts_enc @@ Contact.delete (Id.mk id)
  | Contact Ccreate c -> print contact_enc @@ Contact.create ~name:c.name ~email:c.email
  | Signature Slist -> print signatures_enc @@ Signature.list ()
  | Signature Scount -> print Json_encoding.int @@ Signature.count ()
  | Signature Sget id -> print signature_enc @@ Signature.get (Id.mk id)
  | Signature Screate i ->
    let r = [
      recipient ~name:i.s_name i.s_email ] in
    let data = List.map (fun s -> match String.split_on_char ':' s with [k; v] -> k, `String v | _ -> assert false) i.s_data in
    print signature_enc @@ Signature.create ?body:i.s_body ?subject:i.s_subject
      ~data ~templates:i.s_templates ?delivery_type:i.s_delivery_type r
