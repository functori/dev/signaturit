open Common

module T = struct
  type document_status = [
    | `in_queue | `ready | `signing | `completed | `expired
    | `declined | `error | `canceled ] [@@deriving encoding]
  type event_type =[
    | `email_processed | `email_delivered | `email_openend | `email_bounced | `email_deferred
    | `reminder_email_processed | `reminder_email_delivered | `reminder_email_opened
    | `sms_processed | `sms_delivered | `password_sms_processed | `password_sms_delivered
    | `document_opened | `document_signed | `document_completed | `audit_trail_completed
    | `document_declined | `document_expired | `document_canceled | `photo_added
    | `voice_added | `file_added | `photo_id_added | `terms_and_conditions_accepted
    | `content_widgets_filled
    | `other of string
  ] [@@deriving encoding]

  type file = {
    name: string;
    pages: int option;
    size: int;
  } [@@deriving encoding]

  type event = {
    created_at: date;
    type_: event_type; [@key "type"]
  } [@@deriving encoding]

  type datum = {
    key: string;
    value: Json_repr.ezjsonm;
  } [@@deriving encoding]

  type document_content = {
    email: string;
    events: event list;
    file: file;
    name: string;
    status: document_status;
    data: (string * Json_repr.ezjsonm) list; [@assoc] [@dft []]
    decline_reason: string option;
    signature: (id [@encoding EzEncoding.ignore_enc Json_encoding.(obj1 (req "id" id_enc))]) option;
  } [@@deriving encoding]

  type document = document_content idd [@@deriving encoding]

  type signature_content = {
    data: datum list; [@dft []]
    documents: document list;
    url: string option;
    decliner_email: string option;
  } [@@deriving encoding]

  type signature = signature_content idd [@@deriving encoding]

  type signatures = signature list [@@deriving encoding]

  type widget = {
    page: int option;
    left: int option;
    top: int option;
    height: int option;
    width: int option;
    type_: [`date | `image | `check | `radio | `select | `text | `signature] [@enum];
    default: string option;
    editable: bool option;
    word_anchor: string option;
    options: string option;
    required: bool option;
  } [@@deriving encoding]

  type recipient = {
    email: string;
    name: string option;
    phone: string option;
    require_file_attachment: int list; [@dft []]
    require_photo: int list; [@dft []]
    require_photo_id: int list; [@dft []]
    require_sms_validation: bool option;
    sms_code: bool option;
    type_: ([`signer | `vaildator] [@enum]) option; [@key "type"]
    widgets: widget list; [@dft []]
  } [@@deriving encoding]

  let recipient ?name ?phone ?(file_attachment=[]) ?(photo=[]) ?(photo_id=[])
      ?sms_validation ?sms_code ?type_ ?(widgets=[]) email = {
    email; name; phone; require_file_attachment=file_attachment; require_photo=photo;
    require_photo_id=photo_id; require_sms_validation=sms_validation; sms_code;
    type_; widgets }

  type cc = {
    email: string;
    name: string option
  } [@@deriving encoding]

  type signing_mode = [ `sequential | `parallel ] [@enum] [@@deriving encoding]
  type signature_type = [ `simple | `advanced | `smart ] [@enum] [@@deriving encoding]
  type delivery_type = [ `email | `sms | `url ] [@enum] [@@deriving encoding]

  type signature_input = {
    body: string option;
    subject: string option;
    recipients: recipient list;
    templates: string list; [@dft []]
    callback_url: string option;
    data: (string * Json_repr.ezjsonm) list; [@assoc]
    events_url: string option;
    expire_time: int option;
    branding_id: id option;
    cc: cc list; [@dft []]
    reminders: [`list of int list | `once of int] option;
    signing_mode: signing_mode option;
    type_: signature_type option; [@key "type"]
    delivery_type: delivery_type option;
  } [@@deriving encoding]

  type hook = {
    created_at: date;
    typ: event_type; [@key "type"]
    document: document;
  } [@@deriving encoding]

end
open T
type count = (int [@wrap "count"]) [@@deriving encoding]

let%get signatures = { path="/signatures.json"; errors; security; output=signatures_enc }
let%get signatures_count = { path="/signatures/count.json"; errors; security; output=count_enc }
let%get signature = { path="/signatures/{arg_id_json}"; errors; security; output=signature_enc }
let%post create_signature = { path="/signatures.json" ; errors; security; output=signature_enc; input=signature_input_enc }
let%post signature_reminder = { path="/signatures/{arg_id}/reminder.json"; errors; security; output=signature_enc }
let%post cancel_signature = { path="/signatures/{arg_id}/cancel.json"; errors; security; output=signature_enc }
let%get download_document = {
  path="/signatures/{arg_id}/documents/{arg_id}/download/signed";
  errors; security; raw_output=["application/pdf"] }

let list () = EzReq_lwt.get0 ~headers:(headers ()) ?msg:(msg ()) !api signatures
let count () = EzReq_lwt.get0 ~headers:(headers ()) ?msg:(msg ()) !api signatures_count
let get id = EzReq_lwt.get1 ~headers:(headers ()) ?msg:(msg ()) !api signature id

let create ?body ?subject ?(templates=[]) ?callback_url ?events_url ?expire_time
    ?branding_id ?(cc=[]) ?reminders ?signing_mode ?type_ ?(data=[]) ?delivery_type
    recipients =
  let input = {
    body; subject; recipients; templates; callback_url; data; events_url;
    expire_time; branding_id; cc; reminders; signing_mode; type_;
    delivery_type } in
  EzReq_lwt.post0 ~input ~headers:(headers ()) ?msg:(msg ()) !api create_signature

let remind id =
  EzReq_lwt.post1 ~input:() ~headers:(headers ()) ?msg:(msg ()) !api signature_reminder id
let cancel id =
  EzReq_lwt.post1 ~input:() ~headers:(headers ()) ?msg:(msg ()) !api cancel_signature id
let download ~signature ~document =
  EzReq_lwt.get2 ~headers:(headers ()) ?msg:(msg ()) !api download_document signature document
