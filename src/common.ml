type error = {
  status_code: int;
  message: string;
} [@@deriving encoding]

module type Date = sig
  type t = private string
  val enc : t Json_encoding.encoding
  val mk : string -> t
end
module Date : Date = struct
  type t = string [@@deriving encoding]
  let mk s = s
end
type date = Date.t [@@deriving encoding]

module type Id = sig
  type t = private string
  val enc : t Json_encoding.encoding
  val mk : string -> t
end
module Id : Id = struct
  type t = string [@@deriving encoding]
  let mk s = s
end
type id = Id.t [@@deriving encoding]

type 'a idd = {
  id: id;
  created_at: date;
  content: 'a; [@merge]
} [@@deriving encoding]

let rok = Lwt.return_ok
let rerr = Lwt.return_error
let (let>) = Lwt.bind
let (let|>) p f = Lwt.map f p
let (let>?) p f = Lwt.bind p (Result.fold ~ok:f ~error:rerr)
let (let|>?) p f = Lwt.map (Result.map f) p

let api = ref (EzAPI.BASE "https://api.signaturit.app/v3")
let access_token = ref ""
let verbose = ref false
let set_verbose ?(v=true) () = verbose := v

let headers () =
  [ "Authorization", Format.sprintf "Bearer %s" !access_token ]

let msg () = if !verbose then Some "signaturit" else None

let ecase code name =
  EzAPI.Err.make ~code ~name ~encoding:error_enc ~select:Option.some ~deselect:Fun.id

let errors = List.map2 ecase [400; 401; 403; 404; 429; 500; 503]
    [ "Bad Request"; "Unauthorized"; "Forbidden"; "Not Found"; "Too Many Requests";
      "Internal Server Error"; "Service Unavailable" ]

let security : EzAPI.Security.bearer list =
  [ `Bearer {EzAPI.Security.bearer_name="Bearer"; format=None} ]

let arg_id = EzAPI.Arg.make ~name:"id"
    ~construct:(fun (s: id) -> (s :> string)) ~destruct:(fun s -> Ok (Id.mk s)) ()
let arg_id_json = EzAPI.Arg.make ~name:"id.json"
    ~construct:(fun (s: id) -> (s :> string)^".json")
    ~destruct:(fun s -> Ok (Id.mk @@ Filename.remove_extension s)) ()
