open Common

module T = struct
  type contact_content = {
    email: string;
    name: string;
  } [@@deriving encoding]

  type contact = contact_content idd [@@deriving encoding]
  type contacts = contact list [@@deriving encoding]

  type contact_update = {
    u_email: string option;
    u_name: string option;
  } [@@deriving encoding]
end
open T

let%get contacts = { path="/contacts.json"; errors; security; output=contacts_enc }
let%get contact = { path="/contacts/{arg_id_json}"; errors; security; output=contact_enc }
let%post create_contact = { path="/contacts.json"; errors; security; input=contact_content_enc; output=contact_enc }
let%patch update_contact = { path="/contacts/{arg_id_json}"; errors; security; output=contact_enc; input=contact_update_enc }
let%delete delete_contact = { path="/contacts/{arg_id_json}"; errors; security; output=contacts_enc }

let list () = EzReq_lwt.get0 ~headers:(headers ()) !api contacts
let get id = EzReq_lwt.get1 ~headers:(headers ()) !api contact id
let delete id = EzReq_lwt.get1 ~headers:(headers ()) !api delete_contact id

let create ~name ~email =
  EzReq_lwt.post0 ~headers:(headers ()) ~input:{name; email} ?msg:(msg ()) !api create_contact

let update ?name:u_name ?email:u_email id =
  EzReq_lwt.post1 ~headers:(headers ()) ~input:{u_name; u_email} ?msg:(msg ()) !api update_contact id
